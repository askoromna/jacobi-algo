﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace JacobiAlgo
{
    class Jacobi
    {
        List<double[,]> list = new List<double[,]>();
        SaveFileDialog saveFileDialog1 = new SaveFileDialog();

        public Jacobi()
        {
        }

        public double [,] eigenVector (int N)
        {
            
            double[,] u1 = new double[N, N];
            if (list.Count < 2) u1 = list.ElementAt(0);
            else
            {
                for (int i = list.Count - 1; i > 0; i--)
                {

                    for (int x = 0; x < N; ++x)
                    {
                        for (int y = 0; y < N; ++y)
                        {
                            u1[x, y] += list.ElementAt(i - 1)[x, y] * list.ElementAt(i)[y, x];

                        }
                    }

                }
            }
            

            return u1;
        }

        public void saveResult (double [,] a, double [,] u, int f)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.InitialDirectory = Application.StartupPath;
            int m = findMax(u, f)[0];
            int n = findMax(u, f)[1];
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    StreamWriter sw = new StreamWriter(saveFileDialog1.FileName.ToString());
                    sw.WriteLine("Власні значення вхідної матриці:");
                    for (int i = 0; i < f; i++)
                    {
                        sw.WriteLine("lambda_" + (i + 1) + " = " + a[i, i]);
                    }
                    sw.WriteLine("\nВласні вектори вхідної матриці:");
                    for (int i = 0; i < f; i++)
                    {
                         sw.Write("V_" + (i + 1) + " = (" );
                         for (int j = 0; j < f; j++)
                         {
                             sw.Write(u[j, i]/u[m,n] + "; ");
                         }
                         sw.Write(")\n");
                    }
                    sw.Close();
                }
                catch (Exception ex) { MessageBox.Show("Неможливо зберегти до файлу.\n Original error: " + ex.Message);
                return;
                }
            }

        }

        public double sum(double[,] a, int N)
        {
            double sum = 0;
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    if (i != j) { sum += Math.Pow(a[i, j], 2); }
                }
            }
            return sum;
        }

        public int[] findMax(double[,] a, int N)
        {
            double max = 0;
            int[] m;
            m = new int[2];
      
            for (int i = 0; i < N; i++)             ///find max non-diagonal element
            {
                for (int j = 0; j < N; j++)
                {
                    if ((i!=j)&&(Math.Abs(a[i, j]) > Math.Abs(max)))
                    {
                        max = a[i, j];
                        m[0] = i;
                        m[1] = j;
                    }
                }
            }
            return m;
        }

        public double[,] rotation(double[,] a, int N)
        {
            double[,] u = new double[N, N];
            int i = findMax(a, N)[0];
            int j = findMax(a, N)[1];
           
            double fi = 0.5 * Math.Atan(2.0 * a[i, j] / (a[i, i] - a[j, j]));      // find angle fi 
            
            double[,] b = new double[N, N];
            for (int x = 0; x < N; ++x)
            {
                for (int y = 0; y < N; ++y)
                {
                    b[x, y] = a[x, y];
                    if (x == y) u[x, y] = 1;
                    else u[x, y] = 0;
                }
            }
            u[i, i] = Math.Cos(fi);
            u[j, j] = Math.Cos(fi);
            u[i, j] = -Math.Sin(fi);
            u[j, i] = Math.Sin(fi);
            list.Add(u);


            for (int m = 0; m < N; m++)
            {
                b[m, i] = a[m, i] * Math.Cos(fi) + a[m, j] * Math.Sin(fi);      //формули (2)
                b[m, j] = a[m, j] * Math.Cos(fi) - a[m, i] * Math.Sin(fi);
            }

            for (int x = 0; x < N; ++x)
                for (int y = 0; y < N; ++y)
                    a[x, y] = b[x, y];

            for (int m = 0; m < N; m++)
            {
                a[i, m] = Math.Cos(fi) * b[i, m] + Math.Sin(fi) * b[j, m];      //формули (3)
                a[j, m] = Math.Cos(fi) * b[j, m] - Math.Sin(fi) * b[i, m];
            }
            return a;
        }
    }
}