﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows;
using System.Collections;



namespace JacobiAlgo
{
    public partial class Form1 : Form
    {
        int f = 0;
        double[,] a; //исходная матрица
        double[,] u; //матрица собственных векторов
        Jacobi jacobi = new Jacobi();

        public Form1()
        {
            
            InitializeComponent();
            f = (int)numericUpDown1.Value;

            dataGridView1.RowCount = f;
            dataGridView1.ColumnCount = f;
            dataGridView1.AllowUserToAddRows = false; //чтобы не создавалась пустая строка

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random x = new Random();
            for (int i = 0; i < f; i++)
            {
                for (int j = i; j < f; j++)
                {
                    dataGridView1[j, i].Value = Math.Round(x.NextDouble() * 20 - 10, 2);
                    dataGridView1[i, j].Value = dataGridView1[j, i].Value;
                    
                }
            }
            try
            {
                for (int i = 0; i < f; i++)
                {
                    for (int j = i; j < f; j++)
                    {
                        a[i, j] = Convert.ToDouble(dataGridView1[j, i].Value);
                    }
                }
            }
            catch { return; }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.Columns.Clear(); //удаляем ранее созданые столбцы
            f = (int)numericUpDown1.Value; //размер матрицы
          
            for (int i = 0; i < f; i++)
            {
                dataGridView1.Columns.Add("column" + i, "column" + i); //добавляем столбцы
                dataGridView1.Rows.Add(); //добавляем строк
                var ccol = dataGridView1.Columns[dataGridView1.Columns.Count - 1];
                ccol.Width = 60;
            }

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            u = new double[f, f];
            a = new double[f, f]; //создаем матрицу
            int k = 0; 
            textBox2.Clear();
            textBox1.Clear();

            double eps = Convert.ToDouble(textBox3.Text);

            for (int i = 0; i < f; i++)
                {
                    for (int j = 0; j < f; j++)
                    {
                        try
                        {
                            a[i, j] = Convert.ToDouble(dataGridView1[i, j].Value.ToString());
                        }
                        catch (Exception ex)
                        {
                            dataGridView1.ClearSelection(); //убираем выделение всех ячеек
                            dataGridView1.CurrentCell.Selected = true; //выделяем ячейку с ошибкой
                            MessageBox.Show("Введено невірний символ!" + ex.Message);
                            return;

                        }
                    }
                }
            
                for (int i = 0; i < f; i++)
                {
                    for (int j = i + 1; j < f; j++)
                    {
                        if (a[i, j] != a[j, i])
                        {
                            MessageBox.Show("Матриця не симетрична!\n\nP.S.Симетричною називають квадратну матрицю, елементи якої симетричні відносно головної діагоналі");
                            return;
                        }
                    }
                }
            
                
            double sum = jacobi.sum(a, f);
            while (sum >= eps)
            {
                a = jacobi.rotation(a, f);
                sum = jacobi.sum(a, f);
                k++;  
            }

            u = jacobi.eigenVector(f);
            int m = jacobi.findMax(u, f)[0];
            int n = jacobi.findMax(u, f)[1];

            for (int i = 0; i < f; i++)
            {                                   ////////////////// WHY!??????????????????????????? HOW!!?????????????????
                try
                {
                    textBox1.Text +=  String.Format("{0, -20: F8}", Math.Round(a[i, i],7).ToString()); //TODO: Add format string
                    textBox2.Text += "\n";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неможливо визначити власні вектори.\n Original Error:" + ex.Message);
                    return;
                }
                try
                {
                    for (int j = 0; j < f; j++)
                    {
                        textBox2.Text += String.Format("{0, -20:#.#####}", Math.Round((u[i, j]) / (u[m, n]), 5));            //собственные вектора
                    }
                    textBox2.Text += "\r\n";
                } catch (Exception ex)
                {
                    MessageBox.Show("Неможливо визначити власні вектори.\n Original Error:"+ex.Message);
                    return;
                }
            }
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            textBox1.Clear();
            numericUpDown1.Value = 0;
            dataGridView1.ColumnCount =1;
            dataGridView1.RowCount = 0;
            numericUpDown1.Enabled = true;

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            jacobi.saveResult(a, u, f);
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                button3.PerformClick();
                try
                {
                    StreamReader reader = new StreamReader(openFileDialog1.FileName.ToString());
                    string s;
                    int row = 0;
                    do
                    {
                        s = reader.ReadLine();
                        if (s != null)
                        {
                            string[] a = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                            dataGridView1.Rows.Add(new System.Windows.Forms.DataGridViewRow());
                            dataGridView1.ColumnCount = a.Length;
                            for (int column = 0; column < a.Length; column++)
                            {
                                dataGridView1[column, row].Value = a[column];
                            }
                            row++;
                        }
                    } while (s != null);
                    f = row;
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неможливо зчитати файз з диску.\n Original error: " + ex.Message);
                    return;
                }
                numericUpDown1.Enabled = false;
            }
    }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void довідкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Дана програма виконує обчислення власних значень та власних векторів заданої симетричної матриці.\nІз запитаннями звертатися до автора: Скоромна Анна, anna_skara@mail.ru ");
        }
    }

}